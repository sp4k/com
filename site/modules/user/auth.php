<?php
    use Joomla\Registry\Registry;

    /**
     * Created by PhpStorm.
     * User: Ironman
     * Date: 9/11/2015
     * Time: 7:03 AM
     */


    class Sp4kModulesUserAuth extends Sp4kBaseModel
    {
        public $error = false;
        public $juser;
        public $parent;
        public $account;
        public $result;
        public $parent_juser_id;

        public function execute()
        {

            switch ($context = $this->state->get('context', false)):
                case 'login':
                    $this->executeLogin();
                    continue;
                case 'register':
                    $this->executeRegistration();
                    continue;
                default:
                    continue;
            endswitch;

            return $this;
        }

        public function executeLogin()
        {

            $credentials = [
                'username' => $this->state->get('email'),
                'password' => $this->state->get('password')
            ];

            $options = array('silent'=>false,'action'=>'core.login.site');


            $app = JFactory::getApplication();
            $this->result = $app ->login($credentials,$options);


            //$pluginResponse = new stdClass();
//
            //$pluginResponse->username = $this->state->get('user.email');
            //$pluginResponse->fullname = '';
            //$pluginResponse->password_clear =
            //$pluginResponse->email = $this->state->get('user.email');
            //$this->result = JFactory::getApplication()->triggerEvent('onUserLogin', array((array)$pluginResponse, $options));
        }

        public function executeRegistration()
        {
            $this->createUser();
            if(!$this->error) {
                $this->createAcount();
                if (!$this->error) {
                    $this->createParent();

                    if (!$this->error) {
                        $this->createChildren();

                        if(!$this->error){
                            $this->loginUser();
                            $this->result = 1;
                        }
                    }
                }
            }
        }

        public function createUser()
        {
            $name = $this->state->get('parent.name').' '.$this->state->get('parent.surname');
            $username = $this->state->get('parent.email');
            $password1 = $this->state->get('parent.password');
            $password2 = $this->state->get('parent.passwordConfirmation');
            $email = $this->state->get('parent.email');

            $data = array(
                "name"=>$name,
                "fullname"=>$name,
                "username"=>$username,
                "password"=>$password1,
                "password2"=>$password2,
                "email"=>$email,
                "groups"=>[2]
            );

            $this->juser = $user = clone(JFactory::getUser());
            //Write to database
            if( !($user->bind($data) && $user->save()) ) {
                $this->error =  $user->getError();
            }else{
                JUserHelper::setUserGroups($user->id,[2]);
            }

            //todo test result and set error on fail.
        }

        private function createAcount(){
            $this->account = Sp4kAppsAccountApp::getInstance(
                new Registry()
            )->getItem()->update();
        }

        private function createParent(){
            $this->parent = Sp4kAppsParentApp::getInstance(
                new Registry([
                    'juser_id'=>$this->juser->id,
                    'account_id'=>$this->account->id,
                    'f_name'=>$this->state->get('parent.name'),
                    'l_name'=>$this->state->get('parent.surname'),
                    'primary'=>1,
                    'sms'=>$this->state->get('sms'),
                    'state'=>1
                ])
            )->getItem()->update();

            //todo test result and set error on fail.
        }

        private function createChildren(){
            foreach($this->state->extract('children')->toObject() as $child){
                $child->dob = strtotime($child->dob);
                $child->account_id = $this->account->id;

                $childItem = Sp4kAppsChildApp::getInstance(
                    new Registry($child)
                )->getItem()->update();
            }

            //todo test result and set error on fail.
        }

        private function loginUser(){
            JPluginHelper::importPlugin('user');
            $options = array();
            $options['action'] = 'core.login.site';

            $pluginResponse = new stdClass();
            $pluginResponse->username = $this->juser->username;
            $this->result = JFactory::$application
                ->triggerEvent('onUserLogin', array((array)$pluginResponse, $options));
        }
    }