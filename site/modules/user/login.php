<?php
    /**
     * Created by PhpStorm.
     * User: Ironman
     * Date: 3/10/2016
     * Time: 9:36 AM
     */

    switch ($context = $this->input->get('context', false)):
        case 'test':
            echo 'test';
            continue;
        case 'login':
            $this->processLogin();
            continue;
        case 'register':
            $this->processRegistration();
            continue;
        default:
            continue;
    endswitch;