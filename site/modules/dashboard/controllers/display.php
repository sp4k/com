<?php
/**
 * Created by PhpStorm.
 * User: Ironman
 * Date: 7/16/2015
 * Time: 1:17 PM
 */


    use Joomla\Registry\Registry;

    class Sp4kModulesDashboardControllersDisplay extends Sp4kBaseControllerDisplay
    {
        public function execute()
        {

            $model = new Sp4kModulesDashboardModelsDashboard(
                new Registry()

            );

            $view = new Sp4kModulesDashboardViewsDisplayHtml($model);

            echo $view->render();
        }
    }