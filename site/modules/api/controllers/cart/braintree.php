<?php
    /**
     * Created by PhpStorm.
     * User: Ironman
     * Date: 2/16/2016
     * Time: 1:07 PM
     */
    use Joomla\Registry\Registry;


    /**
     * Class Sp4kModulesApiControllersCartSummary
     *
     * Show the items in the cart session.
     */

    class Sp4kModulesApiControllersCartBraintree extends Sp4kModulesApiControllersBase
    {
        public function execute()
        {
            $response = new stdClass();
            $response->token = Sp4kAppsBraintreeApp::getToken();
            //echo Sp4kAppsBraintreeApp::getToken();
            echo json_encode($response);

            return $this;
        }

    }