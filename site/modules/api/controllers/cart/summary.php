<?php
    /**
     * Created by PhpStorm.
     * User: Ironman
     * Date: 2/16/2016
     * Time: 1:07 PM
     */
    use Joomla\Registry\Registry;


    /**
     * Class Sp4kModulesApiControllersCartSummary
     *
     * Show the items in the cart session.
     */

    class Sp4kModulesApiControllersCartSummary extends Sp4kModulesApiControllersBase
    {
        public function execute()
        {
           $model = Sp4kModulesApiModelsCartSummary::getInstance(new  Registry($this->input->getArray()));
           $response = $model->items;
           echo json_encode($response);

            return $this;
        }

    }