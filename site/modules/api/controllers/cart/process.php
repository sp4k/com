<?php
    /**
     * Created by PhpStorm.
     * User: Ironman
     * Date: 2/16/2016
     * Time: 1:07 PM
     */

use Joomla\Registry\Registry;

    /**
     * Class Sp4kModulesApiControllersCartAdd
     *
     * Process Payments. This needs to support dual payments with.
     */

    class Sp4kModulesApiControllersCartProcess extends Sp4kModulesApiControllersBase
    {
        public function execute()
        {
            $jsonInput = new JInputJSON();

            $model = Sp4kModulesApiModelsCartProcess::getInstance(new  Registry($jsonInput->getArray()));
            $response = $model->getResult();

            // add the result to the Session Cart
            $sessionCart = JFactory::getSession()->get('cart',false,'Sp4k');
            $sessionCart['result'] = $model->getResult();
            echo json_encode($response);

            return $this;
        }

    }