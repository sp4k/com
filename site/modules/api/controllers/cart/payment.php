<?php
    /**
     * Created by PhpStorm.
     * User: Ironman
     * Date: 2/16/2016
     * Time: 1:07 PM
     */
    use Joomla\Registry\Registry;


    /**
     * Class Sp4kModulesApiControllersCartReview
     *
     * Show the cart items and payment information just prior to cart processing
     */
    class Sp4kModulesApiControllersCartPayment extends Sp4kModulesApiControllersBase
    {
        public function execute()
        {
            $model = Sp4kModulesApiModelsCartPayment::getInstance(new  Registry($this->input->getArray()));
            $response = $model->data;
            echo json_encode($response);
            return $this;
        }
    }