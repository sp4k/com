<?php
    /**
     * Created by PhpStorm.
     * User: Ironman
     * Date: 2/16/2016
     * Time: 1:07 PM
     */

    use Joomla\Registry\Registry;

    /**
     * Class Sp4kModulesApiControllersCartAdd
     *
     * Process Payments. This needs to support dual payments with.
     */

    class Sp4kModulesApiControllersCartResult extends Sp4kModulesApiControllersBase
    {
        public function execute(){

            $model = new Sp4kModulesApiModelsCartResult(
                new Registry()
            );

            $response = new stdClass();
            $response->result = $model->result;

            echo json_encode($response);
        }
    }