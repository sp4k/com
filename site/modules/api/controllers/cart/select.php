<?php
    /**
     * Created by PhpStorm.
     * User: Ironman
     * Date: 2/16/2016
     * Time: 1:07 PM
     */


    use Joomla\Registry\Registry;

    /**
     * Class Sp4kModulesApiControllersCartSelect
     *
     * Showing an item for shopper configuration
     */

    class Sp4kModulesApiControllersCartSelect extends Sp4kModulesApiControllersBase
    {
        public function execute()
        {
            if(JFactory::getUser()->guest){
                header("HTTP/1.0 401 UNAUTHORIZED");
                $this->app->close();
            }
            $model = Sp4kModulesApiModelsCartSelect::getInstance(new  Registry($this->input->getArray()));
            $response = $model->product;
            echo json_encode($response);

            return $this;
        }
    }