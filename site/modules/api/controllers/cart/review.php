<?php
    /**
     * Created by PhpStorm.
     * User: Ironman
     * Date: 2/16/2016
     * Time: 1:07 PM
     */
    use Joomla\Registry\Registry;


    /**
     * Class Sp4kModulesApiControllersCartReview
     *
     * Show the cart items and payment information just prior to cart processing
     */
    class Sp4kModulesApiControllersCartReview extends Sp4kModulesApiControllersBase
    {
        public function execute()
        {
           $model = Sp4kModulesApiModelsCartReview::getInstance(new  Registry($this->input->getArray()));
           $response = $model->items;
           echo json_encode($response);

            return $this;
        }
    }