<?php
    /**
     * Created by PhpStorm.
     * User: Ironman
     * Date: 2/16/2016
     * Time: 1:07 PM
     */


    use Joomla\Registry\Registry;
    /**
     * Class Sp4kModulesApiControllersCartAdd
     *
     * Adding item configurations to the cart session.
     */

    class Sp4kModulesApiControllersCartAdd extends Sp4kModulesApiControllersBase
    {
        public function execute()
        {

            $jsonInput = new JInputJSON();

            $model = new Sp4kModulesApiModelsCartAdd(
                new Joomla\Registry\Registry($jsonInput->getArray())
            );

            if(!$model->error){
                $response = json_decode('{"state":1}');
            }
            echo json_encode($response);
        }
    }