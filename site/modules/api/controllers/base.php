<?php
    /**
     * Created by PhpStorm.
     * User: Ironman
     * Date: 2/26/2016
     * Time: 7:07 AM
     */

    class Sp4kModulesApiControllersBase extends JControllerBase
    {

        public function __construct(){

            parent::__construct();
            $app = JFactory::getApplication();
            $app->setHeader('Access-Control-Allow-Origin', '*');

            error_reporting(1);

        }

        public function execute()
        {
            return $this;
        }
    }