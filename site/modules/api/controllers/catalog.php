<?php
    /**
     * Created by PhpStorm.
     * User: Ironman
     * Date: 9/4/2015
     * Time: 7:33 AM
     */


    class Sp4kModulesApiControllersCatalog extends Sp4kModulesApiControllersBase
    {
        public function execute(){

            $model = new Sp4kModulesCatalogModelsItems(
                new Joomla\Registry\Registry($this->input->getArray())
            );

            $response = $model->items;

            echo json_encode($response);

            return $this;
        }
    }
