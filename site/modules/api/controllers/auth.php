<?php
    use Joomla\Registry\Registry;

    /**
     * Created by PhpStorm.
     * User: Ironman
     * Date: 9/11/2015
     * Time: 4:09 PM
     */


    class Sp4kModulesApiControllersAuth extends JControllerBase
    {
        public function execute()
        {
            $jsonInput = new JInputJSON();
            $state = $jsonInput->getArray();
            $state = new Registry(array_merge($state, $this->input->getArray()));

            $model = new Sp4kModulesUserAuth($state);

            $response = new stdClass();
            $response->dataRaw = $model->getState();
            $response->result = $model->result;

            echo json_encode($response);

        }



        /*switch ($context = $this->input->get('context', false)):
               case 'test':
                   echo 'test';
                   continue;
               case 'login':
                   $this->processLogin();
                   continue;
               case 'register':
                   $this->processRegistration();
                   continue;
               default:
                   continue;
           endswitch;*/

        private function processLogin()
        {
            $dataRaw = str_replace('"','',urldecode($this->input->getRaw('data')));
            parse_str($dataRaw,$data);

            $model = new Sp4kModulesUserModelsLogin(
                new Registry($data)
            );

            $response = new stdClass();
            $response->result = $model->result;
        }

        private function processRegistration()
        {

            $dataRaw = str_replace('"','',urldecode($this->input->getRaw('data')));
            parse_str($dataRaw,$data);

            $model = new Sp4kModulesRegistrationModelsAccount(
                new Registry($data)
            );

            $view = new Sp4kModulesRegistrationViewsAccountJson($model);

            if($model->error){
                $view->setLayout('error');
            }

            echo $view->render();
        }
    }