<?php
    /**
     * Created by PhpStorm.
     * User: Ironman
     * Date: 2/22/2016
     * Time: 12:27 PM
     */

    use Joomla\Registry\Registry;

    class Sp4kModulesApiModelsCartSelect extends Sp4kBaseModel
    {
        public $product;

        public static function getInstance( Registry $state){
            return new self($state);
        }

        public function execute()
        {
            $this->setProduct();
        }

        private function setProduct(){
            $this->product = Sp4kAppsProductApp::getInstance(
                new Registry([
                    'id'=>$this->state->get('product_id')
                ])
            )->getItem();

            return $this;
        }
    }

