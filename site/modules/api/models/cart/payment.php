<?php
    use Joomla\Registry\Registry;

    /**
     * Created by PhpStorm.
     * User: Ironman
     * Date: 9/12/2015
     * Time: 5:33 PM
     */

    class Sp4kModulesApiModelsCartPayment extends Sp4kBaseModel
    {
        public $data;
        public $error = false;

        public function getInstance( Registry $state){
            return new self($state);
        }

        public function execute()
        {
            $this->data = new stdClass();
            $cartSession = JFactory::getSession();
            $cart = ($cart = $cartSession->get('cart',[],'Sp4k')) ? $cart : [];

            if(count($cart['items']) > 0){
                $this->items = $cart['items'];
                $this->setPayment();
                $this->paymentMethodToken = Sp4kAppsBraintreeApp::getToken();
            }
        }

        private function setPayment(){
            foreach($this->items as $item){
                foreach($item->product->config->payment as $key=>$payment_frequency_plugin){
                    if($payment_frequency_plugin->enabled){
                        $this->data->payment_frequency_plugins[$key] = $payment_frequency_plugin;
                    }
                }
            }
        }
    }