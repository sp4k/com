<?php
    
    /**
     * Created by Joseph Cardwell.
     * User: Ironman
     * Date: 12/18/2015
     * Time: 3:03 PM
     */

    class Sp4kModulesCatcartControllersDisplay extends JControllerBase
    {
        public function execute(){
            $model = new Sp4kModulesCatcartModelsDisplay();

            $view = new Sp4kModulesCatcartViewsDisplayHtml($model);
            echo $view->render();
        }


    }