<?php

    use Joomla\Registry\Registry;

    /**
     * Created by PhpStorm.
     * User: Ironman
     * Date: 7/16/2015
     * Time: 1:17 PM
     */


    class Sp4kModulesCartControllersAppDisplay extends Sp4kBaseControllerDisplay
    {
        public function execute()
        {
            $model = new Sp4kModulesCartModelsAppDisplay(
                new Registry()
            );

            $view = new Sp4kModulesCartViewsAppHtml($model);

            echo $view->render();
        }
    }