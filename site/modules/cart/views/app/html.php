<?php
/**
 * Created by PhpStorm.
 * User: Ironman
 * Date: 9/11/2015
 * Time: 11:06 AM
 */


    class Sp4kModulesCartViewsAppHtml extends Sp4kBaseViewHtml
    {
        public function __construct(JModel $model, SplPriorityQueue $paths = NULL)
        {
            parent::__construct($model,$paths);
            $this->paths->insert(__DIR__.'/tmpl','normal');
        }

        public function render()
        {
            if(isset($_REQUEST['layout'])){
                $this->setLayout($_REQUEST['layout']);
            }
            return parent::render();
        }
    }