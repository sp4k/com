/**
 * Created by Ironman on 9/3/2015.
 */

angular.module( 'resources.coaches',['ngResource'] ).factory('coachesRestService', function ($resource, appUrl) {
    return $resource(appUrl,
        {
            id: '@id',
            format:'json',
            option:'com_sp4k',
            controller:'api.coaches'
        },
        {
            update:{
                method:'PUT'
            }
        }
        );
});